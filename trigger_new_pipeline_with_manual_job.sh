#!/usr/bin/env bash

set -e

: ${BRANCH:?Need to specify BRANCH}
: ${API_TOKEN:?Need to specify API_TOKEN}
: ${PROJECT_ID:?Need to specify PROJECT_ID}
: ${JOB?:Need to specify JOB}

echo -n "Creating pipeline for project '${PROJECT_ID}'... "
PIPELINE_ID=$(curl -sSL --request POST --header "PRIVATE-TOKEN: $API_TOKEN" "${CI_API_V4_URL}/projects/${PROJECT_ID}/pipeline?ref=${BRANCH}&variables\[\]\[key\]=TRIGGERED_BY_SCRIPT&variables\[\]\[value\]=true" | tee log | jq -r .id)
test -z ${PIPELINE_ID} && (echo "Failed to get PIPELINE_ID" ; exit 1)
echo "${PIPELINE_ID}"

echo -n "Grabbing id for job '${JOB}'... "
JOB_ID=$(curl -sSL --header "PRIVATE-TOKEN: $API_TOKEN" "${CI_API_V4_URL}/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}/jobs?scope[]=manual" | tee log | jq -r ".[] | select(.name==\"$JOB\").id")
jq . log
test -z ${JOB_ID} && (echo "Failed to get JOB_ID" ; exit 1)
: ${JOB_ID?:Failed to get JOB_ID}
echo "${JOB_ID}"

echo -n "Triggering manual job '${JOB}'... "
curl -sSL --request POST --header "PRIVATE-TOKEN: $API_TOKEN" "${CI_API_V4_URL}/projects/${PROJECT_ID}/jobs/${JOB_ID}/play" || echo "FAIL"
echo "OK"
